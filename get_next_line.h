/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 15:44:13 by bbichero          #+#    #+#             */
/*   Updated: 2017/07/25 10:43:36 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include "../libft/libft.h"
# include <fcntl.h>
# define BUF_SIZE 10

char	*ft_get_join(char *s1, char *s2);
int		get_next_line(int const fd, char **line);
int		ft_read(int fd, char **tmp);
void	ft_str(char **line, char **tmp, int *ret);

#endif
